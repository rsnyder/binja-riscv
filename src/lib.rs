#[macro_use]
extern crate log;
extern crate binja;
extern crate riscv_dis;
extern crate rayon;

// Option -> Result
// rework operands/instruction text
// helper func for reading/writing to registers
// do the amo max/min instructions
// Platform api

use std::collections::HashMap;
use std::borrow::Cow;
use std::marker::PhantomData;
use std::fmt;

use binja::architecture;
use binja::architecture::{Architecture, Flag as F, FlagWrite as FW, FlagClass as FC, FlagGroup as FG};
use binja::architecture::ArchitectureExt;
use binja::architecture::CoreArchitecture;
use binja::architecture::CustomArchitectureHandle;
use binja::architecture::InstructionInfo;
use binja::architecture::{Register as Reg, RegisterInfo, ImplicitRegisterExtend, FlagRole, FlagCondition};

use binja::binaryview::{BinaryView, BinaryViewExt, BinaryViewType, BinaryViewTypeExt};
use binja::command;

use binja::llil::{Liftable, LiftedExpr, LiftableWithSize, Mutable, NonSSA, LiftedNonSSA, Label};
use binja::llil;

use riscv_dis::{Op, Instr, RegFile, IntRegType, FloatReg, FloatRegType, RiscVDisassembler};
use riscv_dis::Register as RiscVRegister;

mod liftcheck;

enum RegType {
    Integer(u32),
    Float(u32)
}

#[derive(Copy, Clone)]
struct Register<D: 'static + RiscVDisassembler> {
    id: u32,
    _dis: PhantomData<D>,
}

impl<D: 'static + RiscVDisassembler> Register<D> {
    fn new(id: u32) -> Self {
        Self {
            id,
            _dis: PhantomData,
        }
    }

    fn reg_type(&self) -> RegType {
        let int_reg_count = <D::RegFile as RegFile>::int_reg_count();

        if self.id < int_reg_count {
            RegType::Integer(self.id)
        } else {
            RegType::Float(self.id - int_reg_count)
        }
    }
}

impl<D: 'static + RiscVDisassembler> From<riscv_dis::IntReg<D>> for Register<D> {
    fn from(reg: riscv_dis::IntReg<D>) -> Self {
        Self {
            id: reg.id(),
            _dis: PhantomData,
        }
    }
}

impl<D: 'static + RiscVDisassembler> From<FloatReg<D>> for Register<D> {
    fn from(reg: FloatReg<D>) -> Self {
        let int_reg_count = <D::RegFile as RegFile>::int_reg_count();

        Self {
            id: reg.id() + int_reg_count,
            _dis: PhantomData,
        }
    }
}

impl<D: 'static + RiscVDisassembler> Into<llil::Register<Register<D>>> for Register<D> {
    fn into(self) -> llil::Register<Register<D>> {
        llil::Register::ArchReg(self)
    }
}

impl<D: 'static + RiscVDisassembler> architecture::RegisterInfo for Register<D> {
    type RegType = Self;

    fn parent(&self) -> Option<Self> { None }
    fn offset(&self) -> usize { 0 }

    fn size(&self) -> usize {
        match self.reg_type() {
            RegType::Integer(_) => <D::RegFile as RegFile>::Int::width(),
            RegType::Float(_) => <D::RegFile as RegFile>::Float::width(),
        }
    }

    fn implicit_extend(&self) -> ImplicitRegisterExtend {
        ImplicitRegisterExtend::NoExtend
    }
}

impl<D: 'static + RiscVDisassembler> architecture::Register for Register<D> {
    type InfoType = Self;

    fn name(&self) -> Cow<str> {
        match self.reg_type() {
            RegType::Integer(id) => match id {
                0 => "zero".into(),
                1 => "ra".into(),
                2 => "sp".into(),
                3 => "gp".into(),
                4 => "tp".into(),
                r @ 5 ... 7 => format!("t{}", r - 5).into(),
                r @ 8 ... 9 => format!("s{}", r - 8).into(),
                r @ 10 ... 17 => format!("a{}", r - 10).into(),
                r @ 18 ... 27 => format!("s{}", r - 16).into(),
                r @ 28 ... 31 => format!("t{}", r - 25).into(),
                _ => unreachable!(),
            }
            RegType::Float(id) => match id {
                r @ 0 ... 7 => format!("ft{}", r).into(),
                r @ 8 ... 9 => format!("fs{}", r - 8).into(),
                r @ 10 ... 17 => format!("fa{}", r - 10).into(),
                r @ 18 ... 27 => format!("fs{}", r - 16).into(),
                r @ 28 ... 31 => format!("ft{}", r - 20).into(),
                _ => unreachable!(),
            }
        }
    }

    fn info(&self) -> Self {
        *self
    }

    fn id(&self) -> u32 {
        self.id
    }
}

impl<'a, D: 'static + RiscVDisassembler + Send + Sync> Liftable<'a, RiscVArch<D>> for Register<D> {
    type Result = llil::ValueExpr;

    fn lift(il: &'a llil::Lifter<RiscVArch<D>>, reg: Self)
        -> llil::Expression<'a, RiscVArch<D>, Mutable, NonSSA<LiftedNonSSA>, Self::Result>
    {
        match reg.reg_type() {
            RegType::Integer(0) => {
                il.const_int(reg.size(), 0)
            }
            RegType::Integer(_) => {
                il.reg(reg.size(), reg)
            }
            _ => il.unimplemented()
        }
    }
}

impl<'a, D: 'static + RiscVDisassembler + Send + Sync> LiftableWithSize<'a, RiscVArch<D>> for Register<D> {
    fn lift_with_size(il: &'a llil::Lifter<RiscVArch<D>>, reg: Self, size: usize)
        -> llil::Expression<'a, RiscVArch<D>, Mutable, NonSSA<LiftedNonSSA>, llil::ValueExpr>
    {
        #[cfg(debug_assertions)]
        {
            if reg.size() < size {
                warn!("il @ {:x} attempted to lift {} byte register as {} byte expr",
                      il.current_address(), reg.size(), size);
            }
        }

        match reg.reg_type() {
            RegType::Integer(0) => {
                il.const_int(size, 0)
            }
            RegType::Integer(_) => {
                let expr = il.reg(reg.size(), reg);

                if size < reg.size() {
                    il.low_part(size, expr).into()
                } else {
                    expr
                }
            }
            _ => il.unimplemented()
        }
    }
}

impl<D: 'static + RiscVDisassembler + Send + Sync> fmt::Debug for Register<D> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(self.name().as_ref())
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
struct Flag;

impl architecture::Flag for Flag {
    type FlagClass = Self;

    fn name(&self) -> Cow<str> {
        unreachable!()
    }

    fn role(&self, _class: Option<Self::FlagClass>) -> FlagRole {
        unreachable!()
    }

    fn id(&self) -> u32 {
        unreachable!()
    }
}

impl architecture::FlagWrite for Flag {
    type FlagType = Self;
    type FlagClass = Self;

    fn name(&self) -> Cow<str> {
        unreachable!()
    }

    fn id(&self) -> u32 {
        unreachable!()
    }

    fn class(&self) -> Option<Self> {
        unreachable!()
    }

    fn flags_written(&self) -> Vec<Self::FlagType> {
        unreachable!()
    }
}

impl architecture::FlagClass for Flag {
    fn name(&self) -> Cow<str> {
        unreachable!()
    }

    fn id(&self) -> u32 {
        unreachable!()
    }
}

impl architecture::FlagGroup for Flag {
    type FlagType = Self;
    type FlagClass = Self;

    fn name(&self) -> Cow<str> {
        unreachable!()
    }

    fn id(&self) -> u32 {
        unreachable!()
    }

    fn flags_required(&self) -> Vec<Self::FlagType> {
        unreachable!()
    }

    fn flag_conditions(&self) -> HashMap<Self, FlagCondition> {
        unreachable!()
    }
}

struct RiscVArch<D: 'static + RiscVDisassembler + Send + Sync> {
    handle: CoreArchitecture,
    custom_handle: CustomArchitectureHandle<RiscVArch<D>>,
    _dis: PhantomData<D>,
}

impl<D: 'static + RiscVDisassembler + Send + Sync> architecture::Architecture for RiscVArch<D> {
    type Handle = CustomArchitectureHandle<Self>;

    type RegisterInfo = Register<D>;
    type Register = Register<D>;

    type Flag = Flag;
    type FlagWrite = Flag;
    type FlagClass = Flag;
    type FlagGroup = Flag;

    type InstructionTextContainer = Vec<architecture::InstructionTextToken>;

    fn endianness(&self) -> binja::Endianness {
        binja::Endianness::LittleEndian
    }

    fn address_size(&self) -> usize {
        <D::RegFile as RegFile>::Int::width()
    }

    fn default_integer_size(&self) -> usize {
        <D::RegFile as RegFile>::Int::width()
    }

    fn instruction_alignment(&self) -> usize {
        use riscv_dis::StandardExtension;

        if D::CompressedExtension::supported() {
            2
        } else {
            4
        }
    }

    fn max_instr_len(&self) -> usize {
        4
    }

    fn opcode_display_len(&self) -> usize {
        self.max_instr_len()
    }

    fn associated_arch_by_addr(&self, _addr: &mut u64) -> CoreArchitecture {
        self.handle
    }

    fn instruction_info(&self, data: &[u8], addr: u64) -> Option<InstructionInfo> {
        use architecture::BranchInfo;

        let (inst_len, op) = match D::decode(addr, data) {
            Ok(Instr::Rv16(op)) => (2, op),
            Ok(Instr::Rv32(op)) => (4, op),
            _ => return None,
        };

        let mut res = InstructionInfo::new(inst_len, false);

        match op {
            Op::Jal(ref j) => {
                let target = addr.wrapping_add(j.imm() as i64 as u64);

                let branch = if j.rd().id() == 0 {
                    BranchInfo::Unconditional(target)
                } else {
                    BranchInfo::Call(target)
                };

                res.add_branch(branch, None);
            }
            Op::Jalr(ref i) => {
                // TODO handle the calls with rs1 == 0?
                if i.rd().id() == 0 {
                    let branch_type = if i.rs1().id() == 1 {
                        BranchInfo::FunctionReturn
                    } else {
                        BranchInfo::Unresolved
                    };

                    res.add_branch(branch_type, None);
                }
            }
            Op::Beq(ref b) | Op::Bne(ref b) | Op::Blt(ref b) |
            Op::Bge(ref b) | Op::BltU(ref b) | Op::BgeU(ref b) => {
                res.add_branch(BranchInfo::False(addr.wrapping_add(inst_len as u64)), None);
                res.add_branch(BranchInfo::True(addr.wrapping_add(b.imm() as i64 as u64)), None);
            }
            Op::Ecall => {
                res.add_branch(BranchInfo::SystemCall, None);
            }
            Op::Ebreak => {
                // TODO is this valid, or should lifting handle this?
                res.add_branch(BranchInfo::Unresolved, None);
            }
            Op::Uret | Op::Sret | Op::Mret => {
                res.add_branch(BranchInfo::FunctionReturn, None);
            }
            _ => {}
        }

        Some(res)
    }

    fn instruction_text(&self, data: &[u8], addr: u64) -> Option<(usize, Self::InstructionTextContainer)> {
        use architecture::InstructionTextTokenContents::*;
        use architecture::InstructionTextToken;
        use riscv_dis::Operand;

        let inst = match D::decode(addr, data) {
            Ok(i) => i,
            _ => return None,
        };

        let (inst_len, op) = match inst {
            Instr::Rv16(op) => (2, op),
            Instr::Rv32(op) => (4, op),
        };

        let mut res = Vec::new();
        let mnem = format!("{}", inst.mnem());
        let pad_len = 8usize.saturating_sub(mnem.len());

        // TODO pseudo-instructions

        res.push(InstructionTextToken::new(Instruction, mnem));

        for (i, oper) in inst.operands().iter().enumerate() {
            if i == 0 {
                res.push(InstructionTextToken::new(Text, format!("{:1$}", " ", pad_len)));
            } else {
                res.push(InstructionTextToken::new(OperandSeparator, ","));
                res.push(InstructionTextToken::new(Text, " "));
            }

            match *oper {
                Operand::R(r) => {
                    let reg = self::Register::from(r);

                    res.push(InstructionTextToken::new(Register, reg.name().as_bytes()));
                }
                Operand::F(r) => {
                    let reg = self::Register::from(r);

                    res.push(InstructionTextToken::new(Register, reg.name().as_bytes()));
                }
                Operand::I(i) => {
                    match op {
                        Op::Beq(..) | Op::Bne(..)  | Op::Blt(..)  |
                        Op::Bge(..) | Op::BltU(..) | Op::BgeU(..) |
                        Op::Jal(..) => {
                            // BRANCH or JAL
                            let target = addr.wrapping_add(i as i64 as u64);

                            res.push(InstructionTextToken::new(CodeRelativeAddress(target),
                                                               format!("0x{:x}", target)));
                        }
                        _ => {
                            res.push(InstructionTextToken::new(Integer(i as u64), match i {
                                    -0x8_0000...-1 => format!("-0x{:x}", -i),
                                    _ => format!("0x{:x}", i),
                                }));
                        }
                    }
                }
                Operand::M(i, b) => {
                    let reg = self::Register::from(b);

                    res.push(InstructionTextToken::new(BeginMemoryOperand, ""));
                    res.push(InstructionTextToken::new(Integer(i as u64), if i < 0 {
                            format!("-0x{:x}", -i)
                        } else {
                            format!("0x{:x}", i)
                        }));

                    res.push(InstructionTextToken::new(Text, "("));
                    res.push(InstructionTextToken::new(Register, reg.name().as_bytes()));
                    res.push(InstructionTextToken::new(Text, ")"));
                    res.push(InstructionTextToken::new(EndMemoryOperand, ""));
                }
            }
        }

        Some((inst_len, res))
    }

    fn instruction_llil(&self, data: &[u8], addr: u64, il: &mut llil::Lifter<Self>) -> Option<(usize, bool)> {
        let max_width = self.default_integer_size();

        let (inst_len, op) = match D::decode(addr, data) {
            Ok(Instr::Rv16(op)) => (2, op),
            Ok(Instr::Rv32(op)) => (4, op),
            _ => return None,
        };

        macro_rules! set_reg_or_append_fallback {
            ($op:ident, $t:expr, $f:expr) => ({
                let rd = Register::from($op.rd());
                match rd.id {
                    0 => $f.append(),
                    _ => il.set_reg(rd.size(), rd, $t).append(),
                }
            })
        }

        macro_rules! simple_op {
            ($op:ident, no_discard $f:expr) => ({
                let expr = $f;
                set_reg_or_append_fallback!($op, expr, expr)
            });
            ($op:ident, $f:expr) => {
                set_reg_or_append_fallback!($op, $f, il.nop())
            };
        }

        macro_rules! simple_i {
            ($i:ident, $f:expr ) => ({
                let rs1 = Register::from($i.rs1());
                simple_op!($i, $f(rs1, $i.imm()))
            })
        }

        macro_rules! simple_r {
            ($r:ident, $f:expr ) => ({
                let rs1 = Register::from($r.rs1());
                let rs2 = Register::from($r.rs2());
                simple_op!($r, $f(rs1, rs2))
            })
        }

        match op {
            Op::Load(l) => simple_op!(l, no_discard {
                let size = l.width();
                let rs1 = Register::from(l.rs1());

                let src_expr = il.add(max_width, rs1, l.imm());
                let load_expr = il.load(size, src_expr)
                                  .with_source_operand(1);

                match (size < max_width, l.zx()) {
                    (false,    _) => load_expr,
                    (true,  true) => il.zx(max_width, load_expr).into(),
                    (true, false) => il.sx(max_width, load_expr).into(),
                }
            }),
            Op::Store(s) => {
                let size = s.width();
                let dest = il.add(max_width, Register::from(s.rs1()), s.imm());
                let mut src = il.expression(Register::from(s.rs2()))
                                .with_source_operand(0);

                if size < max_width {
                    src = il.low_part(size, src).into_expr();
                }

                il.store(size, dest, src)
                  .with_source_operand(1)
                  .append();
            }

            Op::AddI(i)  => simple_i!(i, |rs1, imm| il.add(max_width, rs1, imm)),
            Op::SltI(i)  => simple_i!(i, |rs1, imm| il.bool_to_int(max_width, il.cmp_slt(max_width, rs1, imm))),
            Op::SltIU(i) => simple_i!(i, |rs1, imm| il.bool_to_int(max_width, il.cmp_ult(max_width, rs1, imm))),
            Op::XorI(i)  => simple_i!(i, |rs1, imm| il.xor(max_width, rs1, imm)),               
            Op::OrI(i)   => simple_i!(i, |rs1, imm| il.or (max_width, rs1, imm)),               
            Op::AndI(i)  => simple_i!(i, |rs1, imm| il.and(max_width, rs1, imm)),               
            Op::SllI(i)  => simple_i!(i, |rs1, imm| il.lsl(max_width, rs1, imm)),               
            Op::SrlI(i)  => simple_i!(i, |rs1, imm| il.lsr(max_width, rs1, imm)),               
            Op::SraI(i)  => simple_i!(i, |rs1, imm| il.asr(max_width, rs1, imm)),               

            // r-type

            Op::Add(r)  => simple_r!(r, |rs1, rs2| il.add(max_width, rs1, rs2)), 
            Op::Sll(r)  => simple_r!(r, |rs1, rs2| il.lsl(max_width, rs1, rs2)),
            Op::Slt(r)  => simple_r!(r, |rs1, rs2| il.bool_to_int(max_width, il.cmp_slt(max_width, rs1, rs2))),
            Op::SltU(r) => simple_r!(r, |rs1, rs2| il.bool_to_int(max_width, il.cmp_ult(max_width, rs1, rs2))),
            Op::Xor(r)  => simple_r!(r, |rs1, rs2| il.xor(max_width, rs1, rs2)),
            Op::Srl(r)  => simple_r!(r, |rs1, rs2| il.lsr(max_width, rs1, rs2)),
            Op::Or(r)   => simple_r!(r, |rs1, rs2| il.or (max_width, rs1, rs2)),
            Op::And(r)  => simple_r!(r, |rs1, rs2| il.and(max_width, rs1, rs2)),
            Op::Sub(r)  => simple_r!(r, |rs1, rs2| il.sub(max_width, rs1, rs2)),
            Op::Sra(r)  => simple_r!(r, |rs1, rs2| il.asr(max_width, rs1, rs2)),

            // i-type 32-bit

            Op::AddIW(i) => simple_i!(i, |rs1, imm| il.sx(max_width, il.add(4, rs1, imm))),
            Op::SllIW(i) => simple_i!(i, |rs1, imm| il.sx(max_width, il.lsl(4, rs1, imm))),
            Op::SrlIW(i) => simple_i!(i, |rs1, imm| il.sx(max_width, il.lsr(4, rs1, imm))),
            Op::SraIW(i) => simple_i!(i, |rs1, imm| il.sx(max_width, il.asr(4, rs1, imm))),

            // r-type 32-bit

            Op::AddW(r) => simple_r!(r, |rs1, rs2| il.sx(max_width, il.add(4, rs1, rs2))),
            Op::SllW(r) => simple_r!(r, |rs1, rs2| il.sx(max_width, il.lsl(4, rs1, rs2))),
            Op::SrlW(r) => simple_r!(r, |rs1, rs2| il.sx(max_width, il.lsr(4, rs1, rs2))),
            Op::SubW(r) => simple_r!(r, |rs1, rs2| il.sx(max_width, il.sub(4, rs1, rs2))),
            Op::SraW(r) => simple_r!(r, |rs1, rs2| il.sx(max_width, il.asr(4, rs1, rs2))),

            Op::Mul(r) => simple_r!(r, |rs1, rs2| il.mul(max_width, rs1, rs2)),
            /*
            Op::MulH(r) =>
            Op::MulHU(r) =>
            Op::MulHSU(r) =>
            */

            Op::Div(r)  => simple_r!(r, |rs1, rs2| il.divs(max_width, rs1, rs2)),
            Op::DivU(r) => simple_r!(r, |rs1, rs2| il.divu(max_width, rs1, rs2)),
            Op::Rem(r)  => simple_r!(r, |rs1, rs2| il.mods(max_width, rs1, rs2)),
            Op::RemU(r) => simple_r!(r, |rs1, rs2| il.modu(max_width, rs1, rs2)),

            Op::MulW(r)  => simple_r!(r, |rs1, rs2| il.sx(max_width, il.mul (4, rs1, rs2))),
            Op::DivW(r)  => simple_r!(r, |rs1, rs2| il.sx(max_width, il.divs(4, rs1, rs2))),
            Op::DivUW(r) => simple_r!(r, |rs1, rs2| il.sx(max_width, il.divu(4, rs1, rs2))),
            Op::RemW(r)  => simple_r!(r, |rs1, rs2| il.sx(max_width, il.mods(4, rs1, rs2))),
            Op::RemUW(r) => simple_r!(r, |rs1, rs2| il.sx(max_width, il.modu(4, rs1, rs2))),

            Op::Lui(u)   => simple_op!(u, il.const_ptr(u.imm() as i64 as u64)),
            Op::Auipc(u) => simple_op!(u, il.const_ptr(addr.wrapping_add(u.imm() as i64 as u64))),

            Op::Jal(j) => {
                let target = addr.wrapping_add(j.imm() as i64 as u64);

                match (j.rd().id(), il.label_for_address(target)) {
                    (0, Some(l)) => il.goto(l),
                    (0,    None) => il.jump(il.const_ptr(target)),
                    (_,       _) => il.call(il.const_ptr(target)),
                }.append();
            }
            Op::Jalr(i) => {
                let rs1 = i.rs1();
                let imm = i.imm();

                let target = il.add(max_width, Register::from(rs1), imm)
                               .into_expr();

                match (i.rd().id(), rs1.id(), imm) {
                    (0, 1, 0) => il.ret(target),  // jalr zero, ra, 0
                    (0, _, _) => il.jump(target), // indirect jump
                    (_, _, _) => il.call(target), // indirect call
                }.append();
            }

            Op::Beq(b) | Op::Bne(b)  | Op::Blt(b) |
            Op::Bge(b) | Op::BltU(b) | Op::BgeU(b) => {
                let left = Register::from(b.rs1());
                let right = Register::from(b.rs2());

                let cond_expr = match op {
                    Op::Beq(..)  => il.cmp_e  (max_width, left, right),
                    Op::Bne(..)  => il.cmp_ne (max_width, left, right),
                    Op::Blt(..)  => il.cmp_slt(max_width, left, right),
                    Op::Bge(..)  => il.cmp_sge(max_width, left, right),
                    Op::BltU(..) => il.cmp_ult(max_width, left, right),
                    Op::BgeU(..) => il.cmp_uge(max_width, left, right),
                    _ => unreachable!(),
                };

                let mut new_false: Option<Label> = None;
                let mut new_true: Option<Label> = None;

                let ft = addr.wrapping_add(inst_len as u64);
                let tt = addr.wrapping_add(b.imm() as i64 as u64);

                {
                    let f = il.label_for_address(ft).unwrap_or_else(|| {
                        new_false = Some(Label::new());
                        new_false.as_ref().unwrap()
                    });

                    let t = il.label_for_address(tt).unwrap_or_else(|| {
                        new_true = Some(Label::new());
                        new_true.as_ref().unwrap()
                    });

                    il.if_expr(cond_expr, t, f)
                      .append();
                }

                if let Some(t) = new_true.as_mut() {
                    il.mark_label(t);

                    il.jump(il.const_ptr(tt))
                      .append();
                }

                if let Some(f) = new_false.as_mut() {
                    il.mark_label(f);
                }
            }

            Op::Ecall => il.syscall().append(),
            Op::Ebreak => il.bp().append(),
            Op::Uret | Op::Sret | Op::Mret => il.ret(il.unimplemented()).append(),

            Op::Csrrw(i) | Op::Csrrs(i) | Op::Csrrc(i) |
            Op::CsrrwI(i) | Op::CsrrsI(i) | Op::CsrrcI(i) => simple_op!(i, no_discard il.unimplemented()),

            Op::Lr(a) => simple_op!(a, no_discard {
                let size = a.width();
                let load_expr = il.load(size, Register::from(a.rs1()))
                                  .with_source_operand(1);

                match size == max_width {
                    true  => load_expr,
                    false => il.sx(max_width, load_expr).into(),
                }
            }),
            Op::Sc(a) => {
                let size = a.width();
                let rd = a.rd();

                let dest_reg = match rd.id() {
                    0 => llil::Register::Temp(0),
                    _ => Register::from(rd).into(),
                };

                // set rd (or a temp register) to an indeterminate value,
                // which signals to the application whether the conditional
                // store was successful. by clobbering first, we can then
                // emit conditionals based on its value to lift the conditional
                // nature of the store -- dataflow will give up
                il.set_reg(max_width, dest_reg, il.unimplemented())
                  .append();
                                             
                let mut new_false: Option<Label> = None;
                let mut t = Label::new();

                {
                    let cond_expr = il.cmp_e(max_width, dest_reg, 0u64);

                    let ft = addr.wrapping_add(inst_len);
                    let f = il.label_for_address(ft).unwrap_or_else(|| {
                        new_false = Some(Label::new());
                        new_false.as_ref().unwrap()
                    });

                    il.if_expr(cond_expr, &t, f)
                      .append();
                }

                il.mark_label(&mut t);

                il.store(size, Register::from(a.rs1()), Register::from(a.rs2()))
                  .with_source_operand(2)
                  .append();

                if let Some(f) = new_false.as_mut() {
                    il.mark_label(f);
                }
            }
            Op::AmoSwap(a) | Op::AmoAdd(a)  | Op::AmoXor(a)  |
            Op::AmoAnd(a)  | Op::AmoOr(a)   | Op::AmoMin(a)  |
            Op::AmoMax(a)  | Op::AmoMinU(a) | Op::AmoMaxU(a) => {
                let size = a.width();
                let rd = a.rd();
                let rs1 = a.rs1();
                let rs2 = a.rs2();

                let dest_reg = match rd.id() {
                    0 => llil::Register::Temp(0),
                    _ => Register::from(rd).into(),
                };

                let mut next_temp_reg = 1;
                let mut alloc_reg = |rs: riscv_dis::IntReg<D>| match (rs.id(), rd.id()) {
                    (id, r) if id != 0 && id == r => {
                        let reg = llil::Register::Temp(next_temp_reg);
                        next_temp_reg += 1;

                        il.set_reg(max_width, reg, Register::from(rs))
                          .append();

                        reg
                    }
                    _ => Register::from(rs).into()
                };

                let reg_with_address = alloc_reg(rs1);
                let reg_with_val = alloc_reg(rs2);

                let mut load_expr = il.load(size, Register::from(rs1))
                                      .with_source_operand(2);

                if size < max_width {
                    load_expr = il.sx(max_width, load_expr).into();
                }

                il.set_reg(max_width, dest_reg, load_expr)
                  .append();

                let val_expr = LiftableWithSize::lift_with_size(il, reg_with_val, size);
                let dest_reg_val = LiftableWithSize::lift_with_size(il, dest_reg, size);

                let val_to_store = match op {
                    Op::AmoSwap(..) => val_expr,
                    Op::AmoAdd(..)  => il.add(size, dest_reg_val, val_expr).into(),
                    Op::AmoXor(..)  => il.xor(size, dest_reg_val, val_expr).into(),
                    Op::AmoAnd(..)  => il.and(size, dest_reg_val, val_expr).into(),
                    Op::AmoOr(..)   => il.or (size, dest_reg_val, val_expr).into(),
                    Op::AmoMin(..)  |
                    Op::AmoMax(..)  |
                    Op::AmoMinU(..) |
                    Op::AmoMaxU(..) => il.unimplemented(),
                    _ => unreachable!(),
                };

                il.store(size, reg_with_address, val_to_store)
                  .append()
            }

            Op::LoadFp(m) => {
                let rd = Register::from(m.fr());
                let rs1 = Register::from(m.rs1());

                let mem_expr = il.unimplemented_mem(m.width(), il.add(max_width, rs1, m.imm()))
                                 .with_source_operand(1);

                il.set_reg(rd.size(), rd, mem_expr)
                  .append();
            }
            Op::StoreFp(m) => {
                let rs1 = Register::from(m.rs1());
                //let rs2 = m.fr();

                // TODO how to convince it the register gets read?

                let dest_expr = il.add(max_width, rs1, m.imm());

                il.store(m.width(), dest_expr, il.unimplemented())
                  .with_source_operand(1)
                  .append();
            }

            _ => il.unimplemented().append(),
        };

        Some((inst_len as usize, true))
    }

    fn flag_write_llil<'a>(&self, flag: Self::Flag, flag_write: Self::FlagWrite,
                           op: llil::FlagWriteOp<Self::Register>, il: &'a mut llil::Lifter<Self>)
        -> Option<LiftedExpr<'a, Self>>
    {
        None
    }

    fn flag_cond_llil<'a>(&self, cond: FlagCondition, class: Option<Self::Flag>, il: &'a mut llil::Lifter<Self>)
        -> Option<LiftedExpr<'a, Self>>
    {
        None
    }

    fn flag_group_llil<'a>(&self, group: Self::FlagGroup, il: &'a mut llil::Lifter<Self>)
        -> Option<LiftedExpr<'a, Self>>
    {
        None
    }

    fn registers_all(&self) -> Vec<Self::Register> {
        let mut reg_count = <D::RegFile as RegFile>::int_reg_count();

        if <D::RegFile as RegFile>::Float::present() {
            reg_count += 32;
        }

        let mut res = Vec::with_capacity(reg_count as usize);

        for i in 0..reg_count {
            res.push(Register::new(i));
        }

        res
    }

    fn registers_full_width(&self) -> Vec<Self::Register> {
        self.registers_all()
    }

    fn registers_global(&self) -> Vec<Self::Register> {
        let mut regs = Vec::with_capacity(2);

        for i in &[3, 4] {
            regs.push(Register::new(*i));
        }

        regs
    }

    fn registers_system(&self) -> Vec<Self::Register> {
        Vec::new()
    }

    fn flags(&self) -> Vec<Self::Flag> {
        Vec::new()
    }

    fn flag_write_types(&self) -> Vec<Self::FlagWrite> {
        Vec::new()
    }

    fn flag_classes(&self) -> Vec<Self::FlagClass> {
        Vec::new()
    }

    fn flag_groups(&self) -> Vec<Self::FlagGroup> {
        Vec::new()
    }

    fn flags_required_for_flag_condition(&self, cond: FlagCondition, class: Option<Flag>) -> Vec<Self::Flag> {
        Vec::new()
    }

    fn stack_pointer_reg(&self) -> Option<Self::Register> {
        Some(Register::new(2))
    }

    fn link_reg(&self) -> Option<Self::Register> {
        Some(Register::new(1))
    }

    fn register_from_id(&self, id: u32) -> Option<Self::Register> {
        let mut reg_count = <D::RegFile as RegFile>::int_reg_count();

        if <D::RegFile as RegFile>::Float::present() {
            reg_count += 32;
        }

        if id > reg_count {
            None
        } else {
            Some(Register::new(id))
        }
    }

    fn flag_from_id(&self, _id: u32) -> Option<Self::Flag> {
        None
    }

    fn flag_write_from_id(&self, _id: u32) -> Option<Self::FlagWrite> {
        None
    }

    fn flag_class_from_id(&self, _id: u32) -> Option<Self::FlagClass> {
        None
    }

    fn flag_group_from_id(&self, _id: u32) -> Option<Self::FlagGroup> {
        None
    }

    fn handle(&self) -> CustomArchitectureHandle<Self> {
        self.custom_handle
    }
}

impl<D: 'static + RiscVDisassembler + Send + Sync> AsRef<CoreArchitecture> for RiscVArch<D> {
    fn as_ref(&self) -> &CoreArchitecture {
        &self.handle
    }
}


use binja::callingconvention::*;

struct RiscVCC<D: 'static + RiscVDisassembler + Send + Sync> {
    _dis: PhantomData<D>,
}

impl<D: 'static + RiscVDisassembler + Send + Sync> RiscVCC<D> {
    fn new() -> Self {
        RiscVCC {
            _dis: PhantomData,
        }
    }
}

impl<D: 'static + RiscVDisassembler + Send + Sync> CallingConventionBase for RiscVCC<D> {
    type Arch = RiscVArch<D>;

    fn caller_saved_registers(&self) -> Vec<Register<D>> {
        let mut regs = Vec::with_capacity(16);

        for i in &[1u32, 5, 6, 7, 10, 11, 12, 13, 14, 15, 16, 17, 28, 29, 30, 31] {
            regs.push(Register::new(*i));
        }

        regs
    }

    fn callee_saved_registers(&self) -> Vec<Register<D>> { Vec::new() }

    fn int_arg_registers(&self) -> Vec<Register<D>> {
        let mut regs = Vec::with_capacity(8);

        for i in &[10, 11, 12, 13, 14, 15, 16, 17] {
            regs.push(Register::new(*i));
        }

        regs
    }

    fn float_arg_registers(&self) -> Vec<Register<D>> { Vec::new() }
    fn arg_registers_shared_index(&self) -> bool { false }

    fn reserved_stack_space_for_arg_registers(&self) -> bool { false }
    fn stack_adjusted_on_return(&self) -> bool { false }

    fn return_int_reg(&self)     -> Option<Register<D>> { Some(Register::new(10)) } // a0 == x10
    fn return_hi_int_reg(&self)  -> Option<Register<D>> { Some(Register::new(11)) } // a1 == x11
    fn return_float_reg(&self)   -> Option<Register<D>> { None }
    fn global_pointer_reg(&self) -> Option<Register<D>> { Some(Register::new(3)) }  // gp ==  x3

    fn implicitly_defined_registers(&self) -> Vec<Register<D>> { Vec::new() }
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn CorePluginInit() -> bool {
    binja::logger::init(log::LevelFilter::Trace).expect("Failed to set up logging");

    use riscv_dis::{Rv64GRegs, RiscVIMACDisassembler};

    let arch = architecture::register_architecture("Rv64G", |custom_handle, core_arch| {
        RiscVArch::<RiscVIMACDisassembler<Rv64GRegs>> {
            handle: core_arch,
            custom_handle: custom_handle,
            _dis: PhantomData,
        }
    });

    let cc = register_calling_convention(arch, "default", RiscVCC::new());
    arch.set_default_calling_convention(&cc);

    if let Ok(bvt) = BinaryViewType::by_name("ELF") {
        bvt.register_arch(243, binja::Endianness::LittleEndian, arch);
    }

    let plat = arch.standalone_platform().unwrap();

    let syscall_cc = ConventionBuilder::new(arch)
        .caller_saved_registers(&["ra", "t0", "t1", "t2", "t3", "t4", "t5", "t6",
                                  "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7"])
        .int_arg_registers(&["a7", "a0", "a1", "a2", "a3", "a4", "a5", "a6"])
        .return_int_reg("a0")
        .return_hi_int_reg("a1")
        .global_pointer_reg("gp")
        .implicitly_defined_registers(&["gp", "tp"])
        .register("syscall");

    plat.set_syscall_convention(&syscall_cc);

    command::register_for_address("Rust", "aaa", |bv: &BinaryView, addr: u64| { 
        use llil::ExprInfo::*;
        use llil::VisitorAction;

        for block in &bv.basic_blocks_containing(addr) {
            let func = block.function();

            for inst_addr in block.iter() {
                info!("instruction at {:x}", inst_addr);
            }

            if let Ok(llil) = func.low_level_il() {
                if let Some(inst) = llil.instruction_at(addr) {
                    inst.visit_tree(&mut |e, info| {
                        info!("visiting {:?}", e);

                        if let Add(ref op) = *info {
                            let left = op.left().info();
                            let right = op.right().info();

                            if let (Reg(ref r), Const(ref c)) = (left, right) {
                                info!("reg {:?} added to constant {:x} in expr {:?}",
                                      r.source_reg(), c.value(), e);

                                return VisitorAction::Halt;
                            }
                        }

                        VisitorAction::Descend
                    });
                }
            }
        }
    });

    use binja::function::Function;

    command::register_for_function("FlagTesting", "aaaaAAAAAAA", |_bv: &BinaryView, func: &Function| {
        let arch = func.arch();

        for flag in &arch.flags() {
            warn!("flag id {} name {}", flag.id(), flag.name());
        }

        for fw in &arch.flag_write_types() {
            warn!("flag write id {} name {} (class: {})", fw.id(), fw.name(), fw.class().map(|c| c.name().into_owned()).unwrap_or("None".into()));
        }

        for class in &arch.flag_classes() {
            warn!("flag class id {} name {}", class.id(), class.name());
        }

        for group in &arch.flag_groups() {
            warn!("flag group id {} name {}", group.id(), group.name());

            warn!("requires:");
            for flag in group.flags_required() {
                warn!("  {}", flag.name());
            }

            warn!("mapping:");
            for (class, cond) in group.flag_conditions() {
                warn!("  {} -> {:?}", class.name(), cond);
            }
        }

        info!("done");
    });

    command::register_for_function("LiftCheckFunction", "welp", |_bv: &BinaryView, func: &Function| {
        liftcheck::check_function(func);
        info!("liftcheck: Function check completed");
    });

    command::register("LiftCheckAllFunctionsParallel", "welp", liftcheck::check_all_functions_parallel);

    true
}
